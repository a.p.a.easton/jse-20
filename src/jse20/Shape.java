package jse20;

public abstract class Shape {

    protected abstract double getArea();

}
