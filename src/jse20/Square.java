package jse20;

public class Square extends Shape{

    private double a;

    public Square(double a) {
        this.a = a;
    }

    @Override
    protected double getArea() {
        return a*a;
    }
}
