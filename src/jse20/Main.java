package jse20;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Circle> circles = new ArrayList<>();
        addToList(circles, new Circle(3), new Circle(3));
        System.out.println("Area of circles: " + sumArea(circles));

        List<Rectangle> rectangles = new ArrayList<>();
        addToList(rectangles, new Rectangle(2,3), new Rectangle(5,7));
        System.out.println("Area of rectangles: " + sumArea(rectangles));

        List<Square> squares = new ArrayList<>();
        addToList(squares, new Square(2), new Square(3));
        System.out.println("Area of squares: " + sumArea(squares));

        List<Shape> shapes = new ArrayList<>();
        addToList(shapes, new Circle(3), new Rectangle(2,3), new Square(3));
        System.out.println("Area of shapes: " + sumArea(shapes));
    }

    public static <T extends Shape> void addToList(List<T> items, T...shapes){
        items.addAll(Arrays.asList(shapes));
    }

    static double sumArea(List<? extends Shape> collection){
        double result = 0;
        for(Shape shape: collection){
            result += shape.getArea();
        }
        return result;
    }

}
