package jse20;

public class Rectangle extends Shape{

    private double a;
    private double b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    protected double getArea() {
        return a*b;
    }
}
