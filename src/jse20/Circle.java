package jse20;

public class Circle extends Shape{

    private double r;

    public Circle(double r) {
        this.r = r;
    }

    @Override
    protected double getArea() {
        return r*Math.PI;
    }
}
